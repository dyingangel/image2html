document.addEventListener('DOMContentLoaded', function () {

    function createPixel(appendToElement, colorArray, width, pixelSize) {

        var div = document.createElement("div");

        div.style.width = width * pixelSize + 'px';
        div.style.height = pixelSize + 'px';
        div.style.background = "rgba(" + colorArray[0] + "," + colorArray[1] + "," + colorArray[2] + "," + colorArray[3] + ")";
        appendToElement.appendChild(div);
    }

    var pixelSizeinput = document.getElementById("pixelsizeinput");
    var reduceDomOptionInput = document.getElementById("reducedomoption");
    var imageInput = document.getElementById("imageinput");
    var pixelwrapper = document.getElementById("pixelwrapper");
    var canvas = document.getElementById("canvas");
    var image = document.getElementById("image");
    var context = canvas.getContext("2d");
    var dynamicWidth = 1;
    var index, nextIndex, r, g, b, a, r2, g2, b2, a2;
    var pixelSize = 1;
    var reduceDomElements = reduceDomOptionInput.value;

    reduceDomOptionInput.onchange = function () {
        reduceDomElements = this.checked;

        if (reduceDomElements === false) {
            alert('This option will be automatically enabled for large images');
        }
    };

    pixelSizeinput.onchange = function () {
        pixelSize = parseFloat(this.value);
    };

    imageInput.onclick = function () {
        this.value = null;
    };

    imageInput.onchange = function (evt) {

        var tgt = evt.target || window.event.srcElement,
            files = tgt.files;

        //Ensure old stuff was cleared
        pixelwrapper.innerHTML = '';
        image.src = "#";

        //FileReader support
        if (FileReader && files && files.length) {

            var fr = new FileReader();

            fr.onload = function () {
                image.src = fr.result;
            };

            fr.readAsDataURL(files[0]);
        }

        //Not supported
        else {
            // fallback -- perhaps submit the input to an iframe and temporarily store
            // them on the server until the user's session ends.
        }
    };

    image.onload = function () {

        if (image.width * image.height > 250000) {
            reduceDomElements = true;
            reduceDomOptionInput.checked = true;
        }

        context.canvas.width = image.width;
        context.canvas.height = image.height;
        context.drawImage(image, 0, 0, image.width, image.height);

        pixelwrapper.style.width = (image.width * pixelSize) + 'px';
        pixelwrapper.style.height = (image.height * pixelSize) + 'px';

        var imageData = context.getImageData(0, 0, image.width, image.height),
            currentColorHashInRow,
            nextColorHashInRow;

        //Loop through image data
        for (var row = 0, rowLength = canvas.height; row < rowLength; row++) {

            //Reset for each new row
            dynamicWidth = 1;
            nextColorHashInRow = null;

            for (var col = 0, colLength = canvas.width; col < colLength; col++) {

                //Get current pixel
                index = (col + (row * imageData.width)) * 4;
                nextIndex = (col + 1 + (row * imageData.width)) * 4;

                //Separate into color values
                r = imageData.data[index];
                g = imageData.data[index + 1];
                b = imageData.data[index + 2];
                a = imageData.data[index + 3];

                if (reduceDomElements && col + 1 < colLength) {

                    r2 = imageData.data[nextIndex];
                    g2 = imageData.data[nextIndex + 1];
                    b2 = imageData.data[nextIndex + 2];
                    a2 = imageData.data[nextIndex + 3];

                    currentColorHashInRow = '' + r + g + b + a;
                    nextColorHashInRow = '' + r2 + g2 + b2 + a2;

                    if (currentColorHashInRow === nextColorHashInRow) {

                        dynamicWidth++;
                        continue;
                    }
                }

                //Create html pixel and hold last pixel element
                createPixel(pixelwrapper, [r, g, b, a], dynamicWidth, pixelSize);

                dynamicWidth = 1;
            }
        }
    };

}, false);